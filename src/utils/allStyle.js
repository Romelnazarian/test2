// Common styles
import {StyleSheet, Dimensions} from 'react-native';
import fonts from './fonts';
import colors from './colors';
import sizes from './sizes';
const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

export default (allStyles = StyleSheet.create({
  centerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    ...Platform.select({
      ios: {
        fontFamily: fonts.fontFamily_Ios,
      },
      android: {
        fontFamily: fonts.fontFamilyMedium_Android,
      },
    }),
  },
  textBold: {
    ...Platform.select({
      ios: {
        fontFamily: fonts.fontFamily_Ios,
        fontWeight: 'bold',
      },
      android: {
        fontFamily: fonts.fontFamilyBold_Android,
      },
    }),
  },
  
}));
