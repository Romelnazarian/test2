import {Dimensions} from 'react-native';
export default {
  HEIGHT: Dimensions.get('window').height,
  WIDTH: Dimensions.get('window').width,
  button: {
    height: 50,
    width: 330,
    radius: 30,
  },
  input: {
    height: 50,
    width: 350,
    radius: 30,
  },
  icon: {
    height: 22,
    width: 20,
  },

  font: {
    normal: 14,
    middle: 18,
    bold: 22,
  },

  // Space on both sides of the pages
  padding: {
    right: 20,
    left: 20,
    pagination: 50,
  },
  marginBetweenInput: {
    top: 15,
    bottom: 15,
  },

  header:180,
  mainheader:80,
  footer: 70,

  lengthPagination: [1, 2, 3],
  widthPagination: Dimensions.get('window').width - 100,
  heightPagination: 50,
  heightC: 30,
  widthC: 30,

  inProcessHeight: 180,
  titleContainerInProcess: 30,
  imageInProcess: 70,

  userRequestHeight: 200,
  imageRequest: 100,

  listInfoHeight:80,
};
