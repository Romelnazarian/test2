

import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Idcard from 'react-native-vector-icons/AntDesign';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Icon from 'react-native-vector-icons/FontAwesome';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const Login = props => {
  // props.setloading(false);
  const [name, setname] = useState();
  const [nameMessage, setnameMessage] = useState();

  const [phone, setphone] = useState();
  const [PhoneMessage, setPhoneMessage] = useState();


  const arrayValue = [
    {
      id: 1,
      nameField: ['name'],
      value: name == undefined ? '' : name,
    },
    {
        id: 2,
        nameField: ['phone'],
        value: phone == undefined ? '' : phone,
      },
  ];



  const checkValue = () => {
    let all = true;
    arrayValue.map(item => {
      let id = item.id,
        value = item.value,
        nameField = item.nameField;
      let resp = [null, null];
        nameField.map(item => {
          let v = validate(item, value, 'en');
          let checkValidation = v[0];
          let checkValidateMessage = v[1];
          resp[0] = resp[0] || checkValidation ? true : false; // validate is true or false
          resp[1] = checkValidateMessage; // message error
        });
      if (resp[0]) {
        if (id === 1) {
          // name
        //   props.userAction('Code', code);
        } else {
          // phone
        //   props.userAction('phone', phone);
        }
      } else {
        all = false;
        if (id === 1) {
          // name
          setnameMessage(resp[1]);
        }
        else {
            // phone
            setPhoneMessage(resp[1]);
          }
      }
    });

    return all;
  };


  const btn = () => {
    if (checkValue()) {
      props.navigation.navigate('OtpLogin')
    }
  };
  return (
    <View style={[styles.container]}>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={styles.box}>
          <Text
            style={[
              styles.text,
              {textAlign: 'center', fontSize: 22, color: colors.navyblue},
            ]}>
            صفحه ثبت نام
          </Text>

          <InputComponent
            placeholder={'نام خود را وارد کنید'}
            keyboardType={'numerci'}
            maxLength={11}
            onChangeInput={value => setname(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 20,
              height: 55,
            //   borderColor: colors.navyblue,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={nameMessage}
            input={{textAlign: 'right', fontSize: 15, height: 50}}
            icons={
              <Idcard
                name="idcard"
                size={25}
                color={colors.orange}
                style={{position: 'absolute', left: 10}}
              />
            }
          />
          <InputComponent
            placeholder={'شماره تلفن خود را وارد کنید'}
            keyboardType={'numerci'}
            maxLength={11}
            onChangeInput={value => setphone(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 20,
              height: 55,
              borderColor: colors.navyblue,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={PhoneMessage}
            input={{textAlign: 'right', fontSize: 15, height: 50}}
            icons={
              <Mobile
                name="mobile"
                size={35}
                color={colors.orange}
                style={{position: 'absolute', left: 10}}
              />
            }
          />

          <View style={{marginTop: 10, alignSelf: 'center'}}>
            <ButtonComponent
              buttonStyle={{
                borderRadius: 10,
                backgroundColor: colors.green,
                borderColor: colors.navyblue,
              }}
              onClick={() => btn()}
              titleStyle={{fontSize: 16}}
              titleStyle={{color: '#1a2e46'}}
              title={'ثبت نام'}
            />
          </View>
        </View>
        
<Image
source={require('../images/build.jpg')}
style={{
  width: WIDTH,
  height: HEIGHT/3,
  marginTop:'14.9%'
}}
/>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
  },
  box: {
    marginTop: '15%',
    width: '95%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Login;



