// All colors used in the application
export default {
        lightblue:'#d8f0f3',
        navyblue:'#1a2e46',
        white: '#FFFFFF',
        darkGreen: '#1DB954',
        green2:'#0d8e88',
        green: '#3fc4b7',
        shadowGreen: '#9BECAC',
        lightGray: '#F0F0F0',
        lightGray2: '#f8f8f8',
        shadowGray: '#000000',
        gray: '#A9A9A9',
        darkGray:'rgb(115, 115, 115)',
        black: '#2C2C2C',
        // orange: '#C63D48',
        orange: '#f59814',
        red: '#FD7474',
        blue: '#74BFFF',
        darkGray:'rgb(115, 115, 115)',



}