
import React, {useEffect, useState} from 'react';
import {Text, StyleSheet, TouchableOpacity, Animated, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../utils/colors';
import allStyle from '../utils/allStyle';
import sizes from '../utils/sizes';

const buttonComponent = props => {
  // const [animated, setAnimated] = useState(new Animated.Value(0));

  // const animate = () => {
  //   // Will change fadeAnim value to 1 in 5 seconds
  //   Animated.timing(animated, {
  //     toValue: 1,
  //     duration: 400,
  //     useNativeDriver: false,
  //   }).start(() => {
  //     setAnimated(new Animated.Value(0));
  //     props.onClick();
  //   });
  // };
  const onPress = () => {
    props.onClick();
  };
  // const widthAnimation = animated.interpolate({
  //   inputRange: [0, 1],
  //   outputRange: [sizes.button.height, sizes.button.width],
  // });
  // const colorAnimation = animated.interpolate({
  //   inputRange: [0, 0.2, 1],
  //   outputRange: ['rgba(0,0,0,0)', 'rgba(0,0,0,0.2)', 'rgba(0,0,0,0)'],
  // });
  return (
      <TouchableOpacity style={[styles.buttonContainer,{...props.buttonStyle}]} onPress={()=>onPress()}>
           <View>
      {props.iconleft }
        </View>
        <Text style={[styles.textButton, {...props.titleStyle}]}>
          {props.title}
        </Text>
      <View>
      {props.iconright }
        </View>
        {/* <Animated.View
          style={[
            styles.buttonAnimated,
            {
              width: widthAnimation,
              backgroundColor: colorAnimation,
            },
          ]}
        /> */}
      </TouchableOpacity>

  );
};

const styles = StyleSheet.create({
  textButton: {
    ...allStyle.text,
    color: colors.whiteTwo,
    fontSize: sizes.font.bold,
  },
  buttonContainer: {
    height: sizes.button.height,
    width: sizes.button.width,
    borderRadius: sizes.button.radius,
    backgroundColor: colors.orange,
    borderWidth: 1.5,
    borderColor: colors.orange,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    flexDirection:'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: sizes.marginBetweenInput.top,
    marginBottom: sizes.marginBetweenInput.bottom,
  },
  button: {
    height: sizes.button.height,
    width: sizes.button.width,
    borderRadius: sizes.button.radius,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonAnimated: {
    height: sizes.button.height,
    width: sizes.button.height,
    borderRadius: sizes.button.height / 2,
    backgroundColor: 'rgba(0,0,0,0)',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});

export default buttonComponent;
