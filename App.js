import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import Navigation from './src/navigation/navigation';

class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Navigation />
      </View>
    );
  }
}

export default App;
